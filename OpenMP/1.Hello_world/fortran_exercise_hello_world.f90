program exercise_hello_world

    ! @TODO: Include the OpenMP module

    implicit none


    ! @TODO: Insert OpenMP pragma to make the following code block parallel

    ! @TODO: Get the individual thread number in 'id'
    ! using a runtime routine
    integer :: thread_id = 0


    ! Print Hello world from each thread
    write ( *, '(a,i4)' ) 'Hello World from thread ', thread_id

    ! @TODO: Make absolutely sure each thread has printed "Hello World",
    ! before we move forward

    ! @TODO: Now get the master thread to identify itself!
    write ( *, '(a,i4)' ) 'Hi, I am MASTER, my id is always ', thread_id

    ! Now print Hello OpenMP from each thread
    ! (after all everybody needs to learn OpenMP!)
    write ( *, '(a,i4)' ) 'Hello OpenMP from thread  ', thread_id

    stop

end program exercise_hello_world

